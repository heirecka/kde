Title: kde/libkpeople:5 has been renamed to kde-frameworks/kpeople
Author: Heiko Becker <heirecka@exherbo.org>
Content-Type: text/plain
Posted: 2015-03-10
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: kde/libkpeople:5

Please install kde-frameworks/kpeople and *afterwards* uninstall kde/libkpeople:5.

1. Take note of any packages depending on kde/libkpeople:5 :
cave resolve \!kde/libkpeople:5

2. Install kde-frameworks/kpeople:
cave resolve kde-frameworks/kpeople -x

3. Re-install the packages from step 1.

4. Uninstall kde/libkpeople:5 :
cave resolve \!kde/libkpeople:5 -x

Do it in *this* order or you'll potentially *break* your system.
