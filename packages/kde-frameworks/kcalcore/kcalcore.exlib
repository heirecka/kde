# Copyright 2014-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde test-dbus-daemon

SUMMARY="Library providing access to and handling of calendar data"
DESCRIPTION="
It supports the standard formats iCalendar and vCalendar and the group
scheduling standard iTIP.
A calendar can contain information like incidences (events, to-dos, journals),
alarms, time zones, and other useful information. This API provides access to
that calendar information via well known calendar formats iCalendar (or iCal)
and the older vCalendar."

LICENCES="BSD-3 [[ note = [ cmake scripts ] ]] LGPL-2"
SLOT="5"
MYOPTIONS=""

QT_MIN_VER=5.8.0

DEPENDENCIES="
    build:
        sys-devel/bison
    build+run:
        office-libs/libical:=[>=1.0]
        sys-apps/util-linux
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
"

# There are really *many* test, but some need X or kded, others leak *a lot*
# of memory.
RESTRICT="test"

