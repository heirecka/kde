# Copyright 2013-2015 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'qt-4.exlib', which is:
#     Copyright 2008-2010 Bo Ørsted Andresen <zlin@exherbo.org>
#     Copyright 2008-2009, 2010 Ingmar Vanhassel

require qt qmake [ slot=5 ]

export_exlib_phases src_configure src_install

SUMMARY="Qt Cross-platform application framework: QtWebkit"

LICENCES+=" GPL-2"
MYOPTIONS="
    media [[ description = [ HTML5 video/audio support via gstreamer ] ]]

    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

DEPENDENCIES="
    build:
        dev-lang/perl:*
        dev-lang/python:*
        dev-lang/ruby:*
        dev-util/gperf
        sys-devel/bison
        sys-devel/flex
        virtual/pkg-config
    build+run:
        dev-db/leveldb
        dev-db/sqlite:3
        dev-libs/glib:2
        dev-libs/icu:=
        dev-libs/libxml2:2.0
        dev-libs/libxslt
        media-libs/fontconfig
        media-libs/libpng:=
        media-libs/libwebp:=
        sys-libs/zlib
        x11-dri/mesa
        x11-libs/libX11
        x11-libs/libXcomposite
        x11-libs/libXext
        x11-libs/libXrender
        x11-libs/qtbase:${SLOT}[>=${PV}][gui][sql][sqlite]
        x11-libs/qtdeclarative:${SLOT}[>=${PV}]
        x11-libs/qtwebchannel:${SLOT}[>=${PV}]
        media? (
            media-libs/gstreamer:1.0[>=0.10]
            media-plugins/gst-plugins-base:1.0[>=0.10]
        )
        providers:ijg-jpeg? ( media-libs/jpeg:= )
        providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
"

EQMAKE_SOURCES=( WebKit.pro )

qtwebkit_src_configure() {
    local conf_enable conf_disable

    # Previously this was probably done before tarballs were generated and
    # now dropped because qtwebkit is no officially released part of Qt.
    # It fixes a build error where qquickwebpage_p.h can't be found.
    edo /usr/$(exhost --build)/lib/qt5/bin/syncqt.pl \
        -version ${PV} Source/sync.profile

    # TODO: find out how useful qtlocation, qtsensors are
    conf_enable=(
        3d-rendering
        accelerated-2d-canvas
        ftpdir
        inspector
        javascript-debugger
        mathml
        netscape-plugin-api
        sql-database
        svg
        webgl
        xslt
        $(option media && echo use_gstreamer)
        $(option media && echo video)
        $(option media && echo fullscreen_api)
    )

    conf_disable=(
        battery-status
        gamepad
        geolocation
        opencl
        $(option media || echo use_gstreamer)
        $(option media || echo video)
        $(option media || echo fullscreen_api)
    )

    # USE_SYSTEM_MALLOC works around qtwebkit segfaulting:
    # https://bugreports.qt.io/browse/QTBUG-45917
    eqmake CONFIG+="use_system_icu use_system_leveldb" \
        DEFINES+="USE_SYSTEM_MALLOC=1" \
        WEBKIT_CONFIG+="${conf_enable[*]}" \
        WEBKIT_CONFIG-="${conf_disable[*]}"
}

qtwebkit_src_install() {
    default

    # remove references to build dir
    edo sed -i -e "/^QMAKE_PRL_BUILD_DIR/d" "${IMAGE}"/usr/$(exhost --target)/lib/libQt5*.prl
}

