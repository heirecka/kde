# Copyright 2013 Kim Højgaard-Hansen <kimrhh@exherbo.org>
# Copyright 2015-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require plasma kde [ translations='ki18n' ]
require gtk-icon-cache

SUMMARY="GTK2 and GTK3 Configurator for KDE."
DESCRIPTION="
Configuration dialog to adapt GTK applications appearance to your taste under
KDE. Among its many features, it lets you:
- Choose which theme is used for GTK2 and GTK3 applications.
- Tweak some GTK applications behaviour.
- Select what icon theme to use in GTK applications.
- Select GTK applications default fonts.
- Easily browse and install new GTK2 and GTK3 themes.
"

LICENCES="GPL-2 LGPL-2.1"
SLOT="0"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        dev-libs/atk
        dev-libs/glib:2
        kde-frameworks/karchive:5
        kde-frameworks/kcmutils:5
        kde-frameworks/kconfig:5
        kde-frameworks/kconfigwidgets:5
        kde-frameworks/kcoreaddons:5
        kde-frameworks/ki18n:5
        kde-frameworks/kiconthemes:5
        kde-frameworks/kio:5
        kde-frameworks/knewstuff:5
        sys-devel/gettext
        x11-libs/cairo
        x11-libs/gdk-pixbuf:2.0
        x11-libs/gtk+:2
        x11-libs/gtk+:3
        x11-libs/libX11
        x11-libs/libXcursor
        x11-libs/pango
    run:
        kde/kde-cli-tools:4 [[ note = [ kcmshell5 ] ]]
"

if ever at_least 5.11.95 ; then
    DEPENDENCIES+="
        build+run:
            virtual/pkg-config
        build+run:
            gnome-desktop/gsettings-desktop-schemas
    "
fi

