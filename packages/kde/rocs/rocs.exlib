# Copyright 2011 Bernd Steinhauser <berniyh@exherbo.org>
# Copyright 2015-2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde-apps kde [ translations='ki18n' ]
require freedesktop-desktop gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="KDE EDU: Graph Theory IDE"

LICENCES="GPL-2 FDL-1.2 LGPL-2 LGPL-2.1 BSD-2 [[ note = [ cmake scripts ] ]]"
MYOPTIONS=""

KF5_MIN_VER="5.15.0"

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
    build+run:
        dev-libs/boost[>=1.49.0] [[ note = [ rocs can be compiled with boost[>=1.43], but 1.49.0
                is necessary for DOT file support ] ]]
        dev-libs/grantlee:5[>=5.0.0]
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcrash:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdeclarative:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/kparts:5[>=${KF5_MIN_VER}]
        kde-frameworks/kservice:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktexteditor:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=5.4.0]
        x11-libs/qtdeclarative:5[>=5.4.0]
        x11-libs/qtscript:5[>=5.4.0][tools]
        x11-libs/qtsvg:5[>=5.4.0]
        x11-libs/qtwebkit:5[>=5.4.0]
        x11-libs/qtxmlpatterns:5[>=5.4.0]
    run:
        kde/kate:${SLOT}
        x11-libs/qtquickcontrols:5[>=5.4.0]
"

# 10 of 10 tests seem to need a running X server
RESTRICT="test"

rocs_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

rocs_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

