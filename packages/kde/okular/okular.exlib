# Copyright 2011, 2014-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'kdegraphics.exlib', which is:
#     Copyright 2008 Bernd Steinhauser and 2008-2011 Bo Ørsted Andresen

require kde-apps kde [ translations='ki18n' ] freedesktop-desktop gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="An universal document viewer for KDE"

LICENCES="FDL-1.2 GPL-2 LGPL-2"
MYOPTIONS="
    chm        [[ description = [ Support for Microsoft Compiled HTML Help format files ] ]]
    djvu       [[ description = [ Support for DjVu formatted files ] ]]
    encrypted  [[ description = [ Support for encrypted OpenDocument Text documents ] ]]
    epub       [[ description = [ Support for EPub documents ] ]]
    markdown   [[ description = [ Support for Markdown files ] ]]
    mobipocket [[ description = [ Support for displaying Mobipocket e-books ] ]]
    pdf        [[ description = [ Support for PDF (Portable Document Format) files ] ]]
    postscript [[ description = [ Support for PS (PostScript) files ] ]]
    share      [[ description = [ Enable a menu to easily share documents ] ]]
    tiff       [[ description = [ Support for TIFF (Tagged Image File Format) files ] ]]
    tts        [[ description = [ Support for text to speech ] ]]

    ( providers: ijg-jpeg jpeg-turbo ) [[ number-selected = exactly-one ]]
"

KF5_MIN_VER="5.19.0"
QT_MIN_VER="5.6.0"

DEPENDENCIES="
    build:
        kde-frameworks/kdoctools:5[>=${KF5_MIN_VER}]
        virtual/pkg-config
    build+run:
        app-arch/bzip2
        kde-frameworks/kactivities:5[>=${KF5_MIN_VER}]
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kbookmarks:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcompletion:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kjs:5[>=${KF5_MIN_VER}]
        kde-frameworks/kparts:5[>=${KF5_MIN_VER}]
        kde-frameworks/kpty:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwallet:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        kde-frameworks/libkexiv2:5
        kde-frameworks/solid:5[>=${KF5_MIN_VER}]
        kde-frameworks/threadweaver:5[>=${KF5_MIN_VER}]
        media-libs/freetype:2 [[ note = [ Provides freetype font support in the okular DVI generator ] ]]
        media-libs/phonon[qt5]
        sys-libs/zlib [[ note = [ Support for Plucker files in Okular ] ]]
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui]
        x11-libs/qtdeclarative:5[>=${QT_MIN_VER}]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]

        chm? (
            app-arch/libzip
            kde-frameworks/khtml:5[>=${KF5_MIN_VER}]
            media-libs/chmlib
        )
        djvu? ( app-text/djvu[>=3.5.17] )
        encrypted? ( app-crypt/qca:2[>=2.1.0][qt5(-)] )
        epub? ( app-text/ebook-tools )
        markdown? ( app-text/discount[>=2] )
        mobipocket? ( kde/mobipocket:${SLOT}[>=16.11.80] )
        pdf? ( app-text/poppler[>=0.12.1][qt5] )
        postscript? ( app-text/libspectre[>=0.2] )
        (
            providers:ijg-jpeg? ( media-libs/jpeg:= )
            providers:jpeg-turbo? ( media-libs/libjpeg-turbo )
        ) [[ note = [ Support for PalmDB documents in okular ] ]]
        share? ( kde-frameworks/purpose )
        tiff? ( media-libs/tiff )
        tts? ( x11-libs/qtspeech:5[>=${QT_MIN_VER}] )
    run:
        x11-libs/qtgraphicaleffects:5[>=${QT_MIN_VER}]
        x11-libs/qtquickcontrols:5[>=${QT_MIN_VER}]
    recommendation:
        kde/kde-cli-tools:4 [[
            description = [ Launch the webshortcuts KC(control)M(odule) ]
        ]]
    suggestion:
        (
            kde-frameworks/kdeclarative:5 [[ note = [ kpackagelauncherqml ] ]]
            kde-frameworks/kirigami:5
        ) [[ *description = [ Dependencies for the mobile app ] ]]
"

# 12 of 13 tests need a running X server, 1 fails otherwise
RESTRICT="test"

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    CHM
    'chm KF5KHtml'
    'chm LibZip'
    'djvu DjVuLibre'
    'encrypted Qca-qt5'
    EPub
    'markdown Discount'
    'mobipocket QMobipocket'
    'pdf Poppler'
    'postscript LibSpectre'
    'share KDEExperimentalPurpose'
    TIFF
    'tts Qt5TextToSpeech'
)

okular_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

okular_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

