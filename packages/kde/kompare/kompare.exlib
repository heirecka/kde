# Copyright 2013-2017 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'kdesdk.exlib', which is:
#     Copyright 2008-2011 Bo Ørsted Andresen

require kde-apps kde [ translations='ki18n' ] freedesktop-desktop gtk-icon-cache

export_exlib_phases pkg_postinst pkg_postrm

SUMMARY="Diff/Patch Frontend"
DESCRIPTION="Kompare is a GUI front-end program that enables differences between source
files to be viewed and merged. It can be used to compare differences on files or the
contents of folders, and it supports a variety of diff formats and provide many options
to customize the information level displayed."
HOMEPAGE+=" http://www.kde.org/applications/development/${PN}/"

LICENCES="GPL-2 FDL-1.2"
MYOPTIONS=""

KF5_MIN_VER="5.35.0"

DEPENDENCIES+="
    build:
        kde-frameworks/kdoctools:5
    build+run:
        kde/libkomparediff2:${SLOT}[>=17.11.80]
        kde-frameworks/kcodecs:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kparts:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktexteditor:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        x11-libs/qtbase:5[>=5.5.0]
"

kompare_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

kompare_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

