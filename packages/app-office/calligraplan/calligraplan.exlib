# Copyright 2018 Heiko Becker <heirecka@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require kde.org [ subdir=calligra/${PV} ] kde

SUMMARY="A project management application"

HOMEPAGE="https://www.calligra.org/"

LICENCES="BSD-3 FDL-1.2 GPL-2"
SLOT="0"
MYOPTIONS="
    addressbook [[ description = [ Address book support ] ]]
    holidays    [[ description = [ Display public holidays ] ]]
    ical        [[ description = [ Support for Ical export ] ]]
    X
"

KF5_MIN_VER=5.7.0
QT_MIN_VER=5.4.0

DEPENDENCIES="
    build:
        dev-lang/perl:*
    build+run:
        kde/kdiagram[>=2.6.0]
        kde-frameworks/kactivities:5[>=${KF5_MIN_VER}]
        kde-frameworks/karchive:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcmutils:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfig:5[>=${KF5_MIN_VER}]
        kde-frameworks/kconfigwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kcoreaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kdbusaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kguiaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/khtml:5[>=${KF5_MIN_VER}]
        kde-frameworks/ki18n:5[>=${KF5_MIN_VER}]
        kde-frameworks/kiconthemes:5[>=${KF5_MIN_VER}]
        kde-frameworks/kinit:5[>=${KF5_MIN_VER}]
        kde-frameworks/kio:5[>=${KF5_MIN_VER}]
        kde-frameworks/kitemviews:5[>=${KF5_MIN_VER}]
        kde-frameworks/kjobwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/knotifications:5[>=${KF5_MIN_VER}]
        kde-frameworks/kparts:5[>=${KF5_MIN_VER}]
        kde-frameworks/ktextwidgets:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwallet:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwidgetsaddons:5[>=${KF5_MIN_VER}]
        kde-frameworks/kwindowsystem:5[>=${KF5_MIN_VER}]
        kde-frameworks/kxmlgui:5[>=${KF5_MIN_VER}]
        sys-libs/zlib
        x11-libs/qtbase:5[>=${QT_MIN_VER}]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
        addressbook? (
            kde-frameworks/kcontacts:5
            kde-frameworks/akonadi-contact:5
        )
        holidays? (
            kde-frameworks/kholidays:5[>=16.12.3] [[ note = [ aka 5.3.3 ] ]]
        )
        ical? ( kde-frameworks/kcalcore:5 )
        X? (
            x11-libs/libX11
            x11-libs/qtx11extras:5[>=${KF5_MIN_VER}]
        )
        !app-office/calligra:2[<3.1.0] [[
            description = [ Plan has been split out from app-office/calligra ]
            resolution = uninstall-blocked-after
        ]]
"

# At least 5 out of 54 tests need a running display server
RESTRICT="test"

CMAKE_SRC_CONFIGURE_PARAMS+=(
    # support of multiple CPU architectures in one binary - needs Vc
    -DPACKAGERS_BUILD:BOOL=FALSE
)

CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS+=(
    'addressbook KF5Contacts'
    'addressbook KF5AkonadiContact'
    'holidays KF5Holidays'
    'ical KF5CalendarCore'
    'X X11'
)

